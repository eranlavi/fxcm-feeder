﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Configuration;
using System.Threading;
using System.Collections.Concurrent;

using fxcore2;
using System.Xml;

namespace FXCMFeeder
{
    public class FXCMMain
    {
        O2GSession session;
        TableListener tableListener;
        O2GTableManager tableManager;
        SessionStatusListener statusListener;

        UnifeedServer ufs;
        Dictionary<string, string> Symbols;

        public FXCMMain()
        {
            Symbols = new Dictionary<string, string>();

            XmlDocument xDoc = new XmlDocument();
            xDoc.Load("Symbols.xml");
            XmlNodeList nodes = xDoc.SelectNodes("//symbols/symbol");

            foreach (XmlNode node in nodes)
            {
                try
                {
                    Symbols.Add(node.Attributes["in"].Value, node.Attributes["out"].Value);                                      
                }
                catch (Exception ex)
                {
                    Tracer.GetTracer().Error("Fail loading " + node.Attributes["in"].Value + " into Dictionary ", ex);
                }
            }

            ufs = new UnifeedServer();
            ufs.Start();
        }
        
        public void Start()
        {
            session = O2GTransport.createSession();
            session.useTableManager(O2GTableManagerMode.Yes, null);
            statusListener = new SessionStatusListener(session, "", "");
            session.subscribeSessionStatus(statusListener);
            statusListener.Reset();
            session.login(ConfigurationManager.AppSettings["Login"], ConfigurationManager.AppSettings["Password"], ConfigurationManager.AppSettings["URL"], ConfigurationManager.AppSettings["Connection"]);
            if (statusListener.WaitEvents() && statusListener.Connected)
            {
                Tracer.GetTracer().Info("Connected to FXCM server");

                ResponseListener responseListener = new ResponseListener(session);
                session.subscribeResponse(responseListener);

                tableListener = new TableListener();

                tableManager = session.getTableManager();
                O2GTableManagerStatus managerStatus = tableManager.getStatus();
                while (managerStatus == O2GTableManagerStatus.TablesLoading)
                {
                    Thread.Sleep(50);
                    managerStatus = tableManager.getStatus();
                }
                
                if (managerStatus == O2GTableManagerStatus.TablesLoadFailed)
                {
                    Tracer.GetTracer().Error("Cannot refresh all tables of table manager");
                    return;
                }

                SubscribeToSymbol(session, responseListener);

                Thread worker = new Thread(() => SubscribeToUpdatesEvent(tableManager));               
                worker.Start();

                //O2GOffersTable offers = null;
                //offers = (O2GOffersTable)tableManager.getTable(O2GTableType.Offers);
                //offers.RowChanged += offers_RowChanged;
            }
            else
                Tracer.GetTracer().Error("Fail establish connection to FXCM server");
        }

        private void SubscribeToUpdatesEvent(O2GTableManager tableManager)
        {
            O2GOffersTable offers = null;
            offers = (O2GOffersTable)tableManager.getTable(O2GTableType.Offers);
            offers.RowChanged += offers_RowChanged;
        }

        private string ToFastDateTimeString()
        {
            DateTime Date = DateTime.Now;

            char[] chars = new char[24];
            chars[0] = Digit(Date.Day / 10);
            chars[1] = Digit(Date.Day % 10);
            chars[2] = '/';
            chars[3] = Digit(Date.Month / 10);
            chars[4] = Digit(Date.Month % 10);
            chars[5] = '/';
            chars[6] = Digit(Date.Year / 1000);
            chars[7] = Digit(Date.Year % 1000 / 100);
            chars[8] = Digit(Date.Year % 100 / 10);
            chars[9] = Digit(Date.Year % 10);
            chars[10] = '-';
            chars[11] = Digit(Date.Hour / 10);
            chars[12] = Digit(Date.Hour % 10);
            chars[13] = ':';
            chars[14] = Digit(Date.Minute / 10);
            chars[15] = Digit(Date.Minute % 10);
            chars[16] = ':';
            chars[17] = Digit(Date.Second / 10);
            chars[18] = Digit(Date.Second % 10);
            chars[19] = '.';
            chars[20] = Digit(Date.Millisecond / 100);
            chars[21] = Digit(Date.Millisecond % 100 / 10);
            chars[22] = Digit(Date.Millisecond % 10);
            chars[23] = ' ';

            return new string(chars);
        }
        private char Digit(int value) { return (char)(value + '0'); }

        private void offers_RowChanged(object sender, RowEventArgs e)
        {
            O2GOfferTableRow offerTableRow = (O2GOfferTableRow)e.RowData;
            if (offerTableRow != null)
            {
                string name;
                if(Symbols.TryGetValue(offerTableRow.Instrument, out name))
                    ufs.SendData(name + " " + ToFastDateTimeString() + offerTableRow.Bid + " " + offerTableRow.Ask + "\r\n");                
            }
        }

        private O2GRequest CreateSetSubscriptionStatusRequest(O2GSession session, string sOfferID, string sStatus, string Symbol)
        {
            O2GRequest request = null;
            O2GRequestFactory requestFactory = session.getRequestFactory();
            if (requestFactory == null)
            {                
                Tracer.GetTracer().Error("Cannot create request factory");
            }
            O2GValueMap valueMap = requestFactory.createValueMap();
            valueMap.setString(O2GRequestParamsEnum.Command, Constants.Commands.SetSubscriptionStatus);
            valueMap.setString(O2GRequestParamsEnum.SubscriptionStatus, sStatus);
            valueMap.setString(O2GRequestParamsEnum.OfferID, sOfferID);
            request = requestFactory.createOrderRequest(valueMap);
            if (request == null)
            {
                Tracer.GetTracer().Error("Fail Set Subscription Status Request for " + Symbol + ". Error Description: " + requestFactory.getLastError());                
            }
            return request;
        }

        private void SubscribeToSymbol(O2GSession session, ResponseListener responseListener)
        {            
            O2GResponseReaderFactory readerFactory = session.getResponseReaderFactory();
            if (readerFactory == null)
            {
                Tracer.GetTracer().Error("Cannot create request factory");
                return;
            }

            O2GLoginRules loginRules = session.getLoginRules();
            O2GResponse response = loginRules.getTableRefreshResponse(O2GTableType.Offers);
            O2GOffersTableResponseReader offersResponseReader = readerFactory.createOffersTableReader(response);

            string subscribe;
            for (int i = 0; i < offersResponseReader.Count; i++)
            {
                O2GOfferRow offerRow = offersResponseReader.getRow(i);

                if (Symbols.ContainsKey(offerRow.Instrument))
                    subscribe = ConfigurationManager.AppSettings["SymbolSubscribeSign"];
                else
                    subscribe = ConfigurationManager.AppSettings["SymbolDiableSign"];

                O2GRequest request = CreateSetSubscriptionStatusRequest(session, offerRow.OfferID, subscribe, offerRow.Instrument);
                if (request == null)
                {
                    Tracer.GetTracer().Error("Cannot create request for " + offerRow.Instrument);
                    continue;
                }
                responseListener.SetRequestID(request.RequestID);
                session.sendRequest(request);
                if (!responseListener.WaitEvents())
                {
                    Tracer.GetTracer().Error("Subscription status for " + offerRow.Instrument + " has timeout");
                    continue;
                }

                response = responseListener.GetResponse();
                if (response != null && response.Type == O2GResponseType.CommandResponse)
                {
                    Tracer.GetTracer().Info("Subscription status for " + offerRow.Instrument + " is set to " + subscribe);
                }
            }
        }

        public void Close()
        {
            tableListener.UnsubscribeEvents(tableManager);
            statusListener.Reset();
            session.logout();
            statusListener.WaitEvents();
            session.unsubscribeSessionStatus(statusListener);
        }

    }
}
