using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Text;
using System.Threading;
using fxcore2;

namespace FXCMFeeder
{
    class TableListener : IO2GTableListener
    {
        private string mInstrument;
        //private OfferCollection mOffers;

        /// <summary>
        /// ctor
        /// </summary>
        public TableListener()
        {
            //mOffers = new OfferCollection();
        }

        public void SetInstrument(string sInstrument)
        {
            mInstrument = sInstrument;
        }

        #region IO2GTableListener Members

        // Implementation of IO2GTableListener interface public method onAdded
        public void onAdded(string sRowID, O2GRow rowData)
        {
        }

        // Implementation of IO2GTableListener interface public method onChanged
        public void onChanged(string sRowID, O2GRow rowData)
        {            
        }

        // Implementation of IO2GTableListener interface public method onDeleted
        public void onDeleted(string sRowID, O2GRow rowData)
        {
        }

        public void onStatusChanged(O2GTableStatus status)
        {
        }

        #endregion

        public void SubscribeEvents(O2GTableManager manager)
        {
            O2GOffersTable offersTable = (O2GOffersTable)manager.getTable(O2GTableType.Offers);
            offersTable.subscribeUpdate(O2GTableUpdateType.Update, this);
            offersTable.RowChanged += offersTable_RowChanged;
        }

        void offersTable_RowChanged(object sender, RowEventArgs e)
        {
            O2GOfferTableRow offerTableRow = (O2GOfferTableRow)e.RowData;
            if (offerTableRow != null)
            {
                Console.WriteLine("Instrument {0}, Bid={1}, Ask = {2}", offerTableRow.Instrument, offerTableRow.Bid, offerTableRow.Ask);
            }
        }

        public void UnsubscribeEvents(O2GTableManager manager)
        {
            O2GOffersTable offersTable = (O2GOffersTable)manager.getTable(O2GTableType.Offers);
            offersTable.unsubscribeUpdate(O2GTableUpdateType.Update, this);
        }
           
    }
}
