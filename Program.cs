﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FXCMFeeder
{
    class Program
    {       
        static void Main(string[] args)
        {
            try
            {
                System.IO.Directory.CreateDirectory(System.Configuration.ConfigurationManager.AppSettings["BaseLogsDirectory"]);
            }
            catch { }

            Tracer.GetTracer().Info("Starting FXCM Feeder");

            FXCMMain fx = new FXCMMain();
            System.Threading.Thread thread = new System.Threading.Thread(new System.Threading.ThreadStart(fx.Start));
            thread.Start();

            var autoResetEvent = new System.Threading.AutoResetEvent(false);
            Console.CancelKeyPress += (sender, eventArgs) =>
            {
                // cancel the cancellation to allow the program to shutdown cleanly
                eventArgs.Cancel = true;
                autoResetEvent.Set();
            };

            // main blocks here waiting for ctrl-C
            autoResetEvent.WaitOne();

            fx.Close();


        }
    }
}
